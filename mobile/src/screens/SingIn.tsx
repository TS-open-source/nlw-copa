import { VStack, Icon, Text } from 'native-base';
import { Fontisto } from '@expo/vector-icons'
import Logo from '../assets/logo.svg'
import { Button } from "../components/Button";
import { useAuth } from '../hooks/useAuth';


export function SingIn() {

  const { signIn } = useAuth()

  return (
    <VStack flex={1} bgColor="gray.900" alignItems={"center"} justifyContent={"center"} p={7} >
      <Logo width={212} height={40} />
      <Button
        title="ENTRAR COM O GOOGLE"
        leftIcon={<Icon as={Fontisto} name="google" color={"#FFFFFF"} size="md" />}
        type={"SECONDARY"}
        mt={12}
        onPress={signIn}
      />

      <Text color={"#FFFFFF"} textAlign={"center"} p={7} mt={4}>
        Não utilizamos nenhuma informação além {'\n'} do seu e-mail para a criação da sua conta.
      </Text>
    </VStack>
  )
}